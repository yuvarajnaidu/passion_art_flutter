class ApiResponse {
  bool successful;
  String message;
  int code;
  List errorList;
  Object dataResponse;

  bool isSuccessful() {
    return this.successful;
  }

  void setSuccessful(bool successful) {
    this.successful = successful;
  }

  String getMessage() {
    return this.message;
  }

  void setMessage(String message) {
    this.message = message;
  }

  int getCode() {
    return this.code;
  }

  void setCode(int code) {
    this.code = code;
  }

  List getErrorList() {
    return this.errorList;
  }

  void setErrorList(List errorList) {
    this.errorList = errorList;
  }

  Object getDataResponse() {
    return this.dataResponse;
  }

  void setDataResponse(Object dataResponse) {
    this.dataResponse = dataResponse;
  }

  ApiResponse(
      {bool successful,
      String message,
      int code,
      List errorList,
      Object dataResponse}) {
    this.successful = successful;
    this.message = message;
    this.code = code;
    this.errorList = errorList;
    this.dataResponse = dataResponse;
  }
  ApiResponse.fromMap(Map<String, dynamic> data)
      : this(
            successful: data['successful'],
            message: data['message'],
            code: data['code'],
            errorList: data['errorList'],
            dataResponse: data['dataResponse']
            // duration: Duration(minutes: data['duration']),
            // ingredients: new List<String>.from(data['ingredients'])
            );
}
