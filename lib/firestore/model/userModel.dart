class User {
  String id;
  String username;
  String fullName;
  String password;
  String email;
  String recordCreateDate;
  String recordUpdateDate;

  User({String username, String fullName}) {
    this.username = username;
    this.fullName = fullName;
  }

  String getId() {
    return this.id;
  }

  void setId(String id) {
    this.id = id;
  }

  String getUsername() {
    return this.username;
  }

  void setUsername(String username) {
    this.username = username;
  }

  String getPassword() {
    return this.password;
  }

  void setPassword(String password) {
    this.password = password;
  }

  String getFullName() {
    return this.fullName;
  }

  void setFullName(String fullName) {
    this.fullName = fullName;
  }

  String getEmail() {
    return this.email;
  }

  void setEmail(String email) {
    this.email = email;
  }

  String getRecordCreateDate() {
    return this.recordCreateDate;
  }

  void setRecordCreateDate(String recordCreateDate) {
    this.recordCreateDate = recordCreateDate;
  }

  String getRecordUpdateDate() {
    return this.recordUpdateDate;
  }

  void setRecordUpdateDate(String recordUpdateDate) {
    this.recordUpdateDate = recordUpdateDate;
  }

  User.fromMap(Map<dynamic, dynamic> data)
      : this(username: data['username'], fullName: data['full_name']
            // duration: Duration(minutes: data['duration']),
            // ingredients: new List<String>.from(data['ingredients'])
            );

  Map<String, dynamic> toMap(User user) {
    return {
      'username': user.getUsername(),
      'password': user.getPassword(),
      'email': user.getEmail(),
      'fullName': user.getFullName()
    };
  }
}
