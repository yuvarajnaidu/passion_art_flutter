import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart';

Future<Response> axiosGet(String route) async {
  String url = DotEnv().env['CORE_URL'] + route;
  Response response = await get(url);
  return response;
}

Future<Response> axiosPost(String route, Object data) async {
  String url = DotEnv().env['CORE_URL'] + route;
  Response response = await post(url,
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "Authorization": "Some token"
      },
      body: jsonEncode(data));
  return response;
}
