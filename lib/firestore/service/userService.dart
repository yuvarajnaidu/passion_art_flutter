import 'dart:convert';
import 'package:passion_art/firestore/model/userModel.dart';
import 'package:passion_art/firestore/axios/index.dart';
import 'package:passion_art/firestore/model/ApiResponseModel.dart';

Future<ApiResponse> authenticate(String username, String password) async {
  var response = await axiosPost(
      "api/users/authenticate", {"username": username, "password": password});
  return ApiResponse.fromMap(jsonDecode(response.body));
}

Future<ApiResponse> checkUsernameExist(String username) async {
  var response =
      await axiosPost("api/user/usernameCheck", {"username": username});
  return ApiResponse.fromMap(jsonDecode(response.body));
}

Future<ApiResponse> saveUser(User user) async {
  var response = await axiosPost("api/user/saveUser", user.toMap(user));
  return ApiResponse.fromMap(jsonDecode(response.body));
}
