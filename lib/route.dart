import 'package:flutter/material.dart';
import 'package:passion_art/postLogin/home/home.dart';
import 'package:passion_art/main.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    final args = routeSettings.arguments;

    switch (routeSettings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => MyApp());
      case '/home':
        if (args is String) {
          return MaterialPageRoute(builder: (_) => HomeApp(data: args));
        }
        return _errorRoute();
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
          appBar: AppBar(title: Text('Error')),
          body: Center(child: Text('Error')));
    });
  }
}
