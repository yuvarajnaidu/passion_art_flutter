import 'package:flutter/material.dart';

Widget textFormFieldReuse(
    TextEditingController controller,
    bool obscureText,
    String labelText,
    Icon icon,
    IconButton iconButton,
    String errorText,
    TextInputType textInputType,
    Function validator,
    Function onChange) {
  return TextFormField(
    obscureText: obscureText,
    controller: controller,
    keyboardType: textInputType,
    decoration: InputDecoration(
        border: OutlineInputBorder(),
        labelText: labelText,
        suffixIcon: iconButton != null ? iconButton : icon),
    validator: validator != null
        ? validator
        : (value) {
            if (value.isEmpty) {
              return errorText;
            }
            return null;
          },
    onChanged: onChange != null ? onChange : (value) {},
  );
}
