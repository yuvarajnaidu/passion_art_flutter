import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:quiver/async.dart';
import 'package:passion_art/route.dart';
import 'package:passion_art/firestore/service/userService.dart';
import 'package:passion_art/firestore/model/userModel.dart';
import 'package:passion_art/firestore/model/ApiResponseModel.dart';
import 'package:passion_art/component/TextFIeldForm.dart';
import 'package:passion_art/component/ShowDialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future main() async {
  await DotEnv().load('.env');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Passion Art',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Passion Art'),
      initialRoute: '/',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  CardWidget cardWidget;
  Widget cardWidgetImport;
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  void initState() {
    super.initState();
    checkIsLogin();
    cardWidget = CardWidget(this.callback);
    cardWidgetImport = cardWidget;
  }

  Future<Null> checkIsLogin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    if (null != token) {
      Navigator.of(context).pushNamed('/home', arguments: 'test');
    }
  }

  void callback(Widget cardWidgetImport) {
    setState(() {
      this.cardWidgetImport = cardWidgetImport;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(child: cardWidgetImport),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class CardWidget extends StatefulWidget {
  Function callback;

  CardWidget(this.callback);

  @override
  _cardWidgetState createState() => new _cardWidgetState();
}

Duration _timerDuration = new Duration(seconds: 1);

class _cardWidgetState extends State<CardWidget> {
  final username = TextEditingController();
  final password = TextEditingController();
  final confirmPassword = TextEditingController();
  final fullname = TextEditingController();
  final email = TextEditingController();
  bool usernameNotExist = false;
  bool signUpFormEnable = false;
  bool loading = false;
  String headerText = 'Logins';
  bool _obscureText = true;
  Icon usernameIconChangeable = Icon(Icons.account_circle);
  Icon passwordShowHide = Icon(Icons.visibility);
  String textClickChangeForm = 'No Account? Sign Up Now!';
  var countdown;
  int actual;
  bool usernameExist = false;
  bool usernameCheck = false;
  final _formKey = GlobalKey<FormState>();
  final _formKey1 = GlobalKey<FormState>();

  // Username error check
  void userNameNotExist(bool check) {
    setState(() {
      usernameNotExist = check;
    });
  }

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
      if (_obscureText) {
        passwordShowHide = Icon(Icons.visibility_off);
      } else {
        passwordShowHide = Icon(Icons.visibility);
      }
    });
  }

  // Toggles the sign up show and hide
  void signUpFormEnableCheck() {
    setState(() {
      signUpFormEnable = !signUpFormEnable;
      headerText = signUpFormEnable ? 'Sign Up' : 'Login';
      textClickChangeForm = signUpFormEnable
          ? 'Account Already Have? Login Now!'
          : 'No Account? Sign Up Now!';
    });
  }

  // Save user date
  Future<bool> saveUserData(User user) async {
    setState(() {
      loading = true;
    });
    try {
      ApiResponse apiResponse = await saveUser(user);
      setState(() {
        loading = false;
      });
      if (apiResponse.isSuccessful()) {
        resetOnceCompletSignUp();
        return true;
      }
      return false;
    } catch (e) {
      print('Error: $e');
      setState(() {
        loading = false;
      });
      return false;
    }
  }

  // login
  void authenticateUser(String username, String password) async {
    setState(() {
      loading = true;
    });
    try {
      ApiResponse apiResponse = await authenticate(username, password);
      setState(() {
        loading = false;
      });
      if (apiResponse.isSuccessful()) {
        this.userNameNotExist(false);
        if (await setToken(apiResponse.getDataResponse())) {
          Navigator.of(context).pushNamed('/home', arguments: 'test');
        } else {
          print('Internal Error');
        }
      } else {
        this.userNameNotExist(true);
      }
    } catch (e) {
      print('Error: $e');
      setState(() {
        loading = false;
      });
    }
  }

  void triggerUsernameIcon() {
    setState(() {
      usernameIconChangeable = Icon(
        Icons.mood_bad,
        color: Colors.red,
      );
    });
  }

  // Sign up username check
  Future<bool> checkUsernameSignUp() async {
    setState(() {
      usernameCheck = true;
    });
    try {
      ApiResponse apiResponse = await checkUsernameExist(username.text);
      setState(() {
        usernameCheck = false;

        if (apiResponse.isSuccessful()) {
          usernameIconChangeable = Icon(
            Icons.check_circle,
            color: Colors.green,
          );
        } else {
          triggerUsernameIcon();
        }
      });

      return !apiResponse.isSuccessful();
    } catch (e) {
      print('Error: $e');
      setState(() {
        usernameCheck = false;
      });
      return null;
    }
  }

  final snackBar = SnackBar(content: Text('Yay! Sign Up Complete Login Now!'));

  void resetOnceCompletSignUp() {
    email.clearComposing();
    confirmPassword.clearComposing();
    fullname.clearComposing();
    _formKey.currentState.reset();
    _formKey1.currentState.reset();
    setState(() {
      signUpFormEnable = false;
      headerText = 'Login';
    });
  }

  // Timer
  void startTimer() async {
    CountdownTimer countDownTimer = new CountdownTimer(
      new Duration(minutes: 0),
      new Duration(milliseconds: 2000),
    );

    countdown = countDownTimer.listen(null);

    countdown.onDone(() async {
      countdown.cancel();
      usernameExist = await checkUsernameSignUp();
      _formKey1.currentState.validate();
    });
  }

  void usernameOnChange(String value) {
    if (signUpFormEnable) {
      startTimer();
      countdown.cancel();
      if (value.isEmpty || value.contains(' ')) {
        countdown.cancel();
        _formKey1.currentState.validate();
      } else {
        startTimer();
      }
    }
  }

  String validateConfirmPassword(String value) {
    if (value.isEmpty) {
      return 'Please enter your confirm password';
    }
    if (value != password.text) return 'Password Not Match';
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        margin: new EdgeInsets.symmetric(horizontal: 30.0),
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  margin: new EdgeInsets.only(
                      top: 40.0, left: 20.0, right: 20.0, bottom: 20.0),
                  child: Column(children: <Widget>[
                    Text(
                      headerText,
                      style: TextStyle(
                          fontSize: 30.0, fontWeight: FontWeight.bold),
                    ),
                    Container(
                      child: Form(
                        key: _formKey1,
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 10),
                            textFormFieldReuse(
                                username,
                                false,
                                'Username',
                                usernameIconChangeable,
                                null,
                                null,
                                TextInputType.text,
                                usernameValidator,
                                usernameOnChange),
                            Visibility(
                              child: Column(
                                children: <Widget>[
                                  SizedBox(height: 10),
                                  LinearProgressIndicator(),
                                ],
                              ),
                              visible: usernameCheck,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                        child: Form(
                      key: _formKey,
                      child: Column(children: <Widget>[
                        SizedBox(height: 10),
                        Visibility(
                          child: textFormFieldReuse(
                              fullname,
                              false,
                              'Full Name',
                              Icon(Icons.person),
                              null,
                              'Please enter your Full Name',
                              TextInputType.text,
                              null,
                              null),
                          visible: signUpFormEnable,
                        ),
                        SizedBox(height: 10),
                        Visibility(
                          child: textFormFieldReuse(
                              email,
                              false,
                              'Email',
                              Icon(Icons.alternate_email),
                              null,
                              'Please enter your Full Name',
                              TextInputType.emailAddress,
                              validateEmail,
                              null),
                          visible: signUpFormEnable,
                        ),
                        SizedBox(height: 10),
                        textFormFieldReuse(
                            password,
                            _obscureText,
                            'Password',
                            null,
                            IconButton(
                              icon: passwordShowHide,
                              onPressed: () => _toggle(),
                            ),
                            'Please enter your password',
                            TextInputType.text,
                            null,
                            null),
                        SizedBox(height: 10),
                        Visibility(
                          child: textFormFieldReuse(
                              confirmPassword,
                              true,
                              'Confirm Password',
                              Icon(Icons.enhanced_encryption),
                              null,
                              'Please enter your confirm password',
                              TextInputType.text,
                              validateConfirmPassword,
                              null),
                          visible: signUpFormEnable,
                        ),
                      ]),
                    )),
                    SizedBox(height: 10),
                    Visibility(
                      child: Text(
                        'Please check your username and password.',
                        style: TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      ),
                      visible: usernameNotExist,
                    ),
                    GestureDetector(
                      child: Text(
                        textClickChangeForm,
                        style: TextStyle(
                            fontSize: 10.0,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                            color: Colors.blue),
                      ),
                      onTap: () {
                        signUpFormEnableCheck();
                        _formKey.currentState.reset();
                      },
                    )
                  ])),
              ButtonBar(
                children: <Widget>[
                  Visibility(
                    child: AbsorbPointer(
                      absorbing: loading,
                      child: RaisedButton(
                          child: Text(headerText),
                          color: Colors.blue,
                          onPressed: () async {
                            if (_formKey.currentState.validate() &&
                                _formKey1.currentState.validate()) {
                              if (signUpFormEnable) {
                                User user = new User();
                                user.setFullName(fullname.text);
                                user.setUsername(username.text);
                                user.setPassword(password.text);
                                user.setEmail(email.text);
                                bool check = await saveUserData(user);
                                await showDialogReuse(context, check);
                                resetOnceCompletSignUp();
                              } else {
                                this.userNameNotExist(false);
                                authenticateUser(username.text, password.text);
                              }
                            }
                          }),
                    ),
                    visible: !loading,
                  ),
                  Visibility(
                    child: CircularProgressIndicator(
                      strokeWidth: 4.0,
                    ),
                    visible: loading,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  String usernameValidator(String value) {
    if (value.isEmpty) {
      triggerUsernameIcon();
      return 'Please enter your username';
    }
    if (value.contains(' ')) {
      triggerUsernameIcon();
      return 'Username cannot have space';
    }
    if (usernameExist) {
      triggerUsernameIcon();
      return 'Username Taken';
    }
    return null;
  }
}

String validateEmail(String value) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(pattern);
  if (!regex.hasMatch(value))
    return 'Enter Valid Email';
  else
    return null;
}

Future<bool> setToken(Map<String, dynamic> data) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  try {
    await prefs.setString('token', data["token"]);
    return true;
  } catch (e) {
    return false;
  }
}
